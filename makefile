############################
# Vertica Analytic Database
#
# Makefile to build example user defined functions
#
# To run under valgrind:
#   make RUN_VALGRIND=1 run
#
# Copyright (c) 2005 - 2012 Vertica, an HP company
############################

## Set to the location of the SDK installation
SDK_HOME?=/opt/vertica/sdk
SDK_JAR?=/opt/vertica/

CXX?=g++
CXXFLAGS:=$(CXXFLAGS) -I $(SDK_HOME)/include -I HelperLibraries -g -Wall -Wno-unused-value -shared -fPIC -std=c++0x 
LIBS = -luuid

ifdef OPTIMIZE
## UDLs should be compiled with compiler optimizations in release builds
CXXFLAGS:=$(CXXFLAGS) -O3
endif

## Set to the location of the simulator installation
SIM_HOME?=/opt/vertica/scripts/sdk/simulator

## Set to the desired destination directory for .so output files
BUILD_DIR?=$(abspath build)

## Set to a valid temporary directory
TMPDIR?=/tmp

## Set to the path to 
BOOST_INCLUDE ?= /usr/include
CURL_INCLUDE ?= /usr/include
ZLIB_INCLUDE ?= /usr/include
BZIP_INCLUDE ?= /usr/include

JDK6_HOME := $(SOURCE)/../third-party/jdk/jdk1.6.0_11
JDK5_HOME := $(SOURCE)/../third-party/jdk/jdk1.5.0_22
JAVA_HOME := $(SOURCE)/../third-party/jre/1.6/

JAVA_PATH := bin/java
JAVAC_PATH := bin/javac
JAR_PATH := bin/jar

JAVA ?= $(JDK6_HOME)/$(JAVA_PATH)
JAVAC ?= $(JDK6_HOME)/$(JAVAC_PATH)
JAR ?= $(JDK6_HOME)/$(JAR_PATH)

ifdef RUN_VALGRIND
VALGRIND=valgrind --leak-check=full
endif

.PHONEY: simulator run ScalarFunctions TransformFunctions AnalyticFunctions AggregateFunctions UserDefinedLoad JavaFunctions

all: IDFunctions

$(BUILD_DIR)/.exists:
	test -d $(BUILD_DIR) || mkdir -p $(BUILD_DIR)
	touch $(BUILD_DIR)/.exists

###
# IDFunctions
###
IDFunctions: $(BUILD_DIR)/IDFunctions.so

$(BUILD_DIR)/IDFunctions.so: *.cpp $(SDK_HOME)/include/Vertica.cpp $(SDK_HOME)/include/BuildInfo.h $(BUILD_DIR)/.exists
	$(CXX) $(CXXFLAGS) -o $@ *.cpp $(SDK_HOME)/include/Vertica.cpp -luuid

clean:
	rm -rf $(TMPDIR)/libcsv-3.0.1
	rm -f $(BUILD_DIR)/*.so 
	rm -f $(BUILD_DIR)/*.jar
	rm -rf $(BUILD_DIR)/Java*
	-rmdir $(BUILD_DIR) >/dev/null 2>&1 || true
