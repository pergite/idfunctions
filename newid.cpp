/* Copyright (c) 1014 johano, 
 *
 */
#include <string>
#include <stdio.h>
#include <vector>
#include <uuid/uuid.h>

#include "Vertica.h"

class exception;
using namespace Vertica;
using namespace std;

struct uuid {
	uint32_t	time_low;
	uint16_t	time_mid;
	uint16_t	time_hi_and_version;
	uint16_t	clock_seq;
	uint8_t	node[6];
};


void uuid_unpack(const uuid_t in, struct uuid *uu)
{
    const uint8_t    *ptr = in;
    uint32_t        tmp;
 
    tmp = *ptr++;
    tmp = (tmp << 8) | *ptr++;
    tmp = (tmp << 8) | *ptr++;
    tmp = (tmp << 8) | *ptr++;
    uu->time_low = tmp;
 
    tmp = *ptr++;
    tmp = (tmp << 8) | *ptr++;
    uu->time_mid = tmp;
    
    tmp = *ptr++;
    tmp = (tmp << 8) | *ptr++;
    uu->time_hi_and_version = tmp;
 
    tmp = *ptr++;
    tmp = (tmp << 8) | *ptr++;
    uu->clock_seq = tmp;
 
    memcpy(uu->node, ptr, 6);
}

void uuid_unparse_x(const uuid_t uu, char *out, const char *fmt)
{
	struct uuid uuid;

	uuid_unpack(uu, &uuid);
	sprintf(out, fmt,
		uuid.time_low, uuid.time_mid, uuid.time_hi_and_version,
		uuid.clock_seq >> 8, uuid.clock_seq & 0xFF,
		uuid.node[0], uuid.node[1], uuid.node[2],
		uuid.node[3], uuid.node[4], uuid.node[5]);
}


/*
 * This is a simple function that returns a new uuid
 */
class NewID : public ScalarFunction
{
public:

    /*
     * This method processes a block of rows in a single invocation.
     */
    virtual void processBlock(ServerInterface &srvInterface,
                              BlockReader &argReader,
                              BlockWriter &resWriter)
    {
        try {
            // Basic error checking
            if (argReader.getNumCols() != 0)
                vt_report_error(0, "Function doesnt accept any arguments, but %zu provided",  argReader.getNumCols());

			char out[32];

            do {
				uuid_t guid;
		        uuid_generate( guid );
        		uuid_unparse_x(guid, out, "%08x%04x%04x%02x%02x%02x%02x%02x%02x%02x%02x");
        
                VString output = resWriter.getStringRef();
				output.copy(out);

                resWriter.next();
            } 
            while (argReader.next());
            
        } catch(std::exception& e) {
            // Standard exception. Quit.
            vt_report_error(0, "Exception while processing block: [%s]", e.what());
        }
    }
};

class NewIDFactory : public ScalarFunctionFactory
{
    // return an instance of NewID to perform the actual addition.
    virtual ScalarFunction *createScalarFunction(ServerInterface &interface)
    { return vt_createFuncObject<NewID>(interface.allocator); }

    virtual void getPrototype(ServerInterface &interface,
                              ColumnTypes &argTypes,
                              ColumnTypes &returnType)
    {
        returnType.addVarchar();
    }

    // Determine the length of the varchar string being returned.
    virtual void getReturnType(ServerInterface &srvInterface,
	                        const SizedColumnTypes &argTypes,
	                        SizedColumnTypes &returnType)
    {
        returnType.addVarchar(32);
    }
};

RegisterFactory(NewIDFactory);
